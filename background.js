var HEADERS_TO_STRIP_LOWERCASE = [
	'content-security-policy',
	'x-frame-options',
	'access-control-allow-origin'
];

function setHeader(e) {
	if (!e.responseHeaders)
		return;
	e.responseHeaders = e.responseHeaders.filter(function (header) {
		return HEADERS_TO_STRIP_LOWERCASE.indexOf(header.name.toLowerCase()) < 0;
	});

	var url = !(e.originUrl || e.initiator) ? new URL("https://accgen.cathook.club") : new URL(e.originUrl || e.initiator);
	e.responseHeaders.push({
		name: "Access-Control-Allow-Origin",
		value: url.host.replace(/^[^.]+\./g, "") == "cathook.club" ? url.origin : "https://accgen.cathook.club"
	});

	if (e.url == "https://store.steampowered.com/join/")
		console.log(e.responseHeaders)
	return {
		responseHeaders: e.responseHeaders
	};
}

(typeof browser != "undefined" ? browser : chrome).webRequest.onHeadersReceived.addListener(setHeader, {
	urls: ["https://*.cathook.club/*", "https://store.steampowered.com/*"]
}, (typeof browser != "undefined" ? ["blocking", "responseHeaders"] : ["blocking", "responseHeaders", "extraHeaders"]));